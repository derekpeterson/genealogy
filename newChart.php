<html>
<head>
	<script type="application/javascript">
		var treedata=new Array();
		var treeid = new Array();
		var level = 0;
		var centerX = 675, centerY = 660;
		var radius = new Array(80, 120, 180, 260, 360, 500, 650);
		
		function on_click(ev)
		{
			var x, y;
			
			x = ev.x;
			y = ev.y;
			// Get the mouse position relative to the canvas element.
			if (ev.layerX || ev.layerX) { //for firefox
				x = ev.layerX;
				y = ev.layerY;
			}
			x-=canvas.offsetLeft;
			y-=canvas.offsetTop;
			
			var len = Math.sqrt(Math.abs(centerX - x) * Math.abs(centerX - x) + Math.abs(centerY - y) * Math.abs(centerY - y));
			var i = 0;
			while (len > radius[i])
				i ++;
			
			var d = Math.PI / Math.pow(2, i);
			var an = -Math.asin((centerX - x)/len);
			
			var st = - Math.PI / 2;
			var j = 0;
			
			while (!(an >= st && an <= st + d))
			{
				st += d;
				j = j + 1;
			}
			
			var ind = 0;
			
			for (var k = 0; k <= i; k ++)
				ind += Math.pow(2, k);
			ind = ind - j;
			
			
			/// index = ind;
			if (treeid[ind])
				window.location.href = "person.php?id=" + treeid[ind];
		}
		
		function getLevel(root, l)
		{
			if (root.childNodes.length > 1)
			{
				if (root.childNodes[1] != null) 
					getLevel(root.childNodes[1], l + 1);
				if (root.childNodes[3] != null) 
					getLevel(root.childNodes[3], l + 1);
			}else if (l > level)
				level = l;
		}
		
		function fillData(root, index)
		{
			treedata[index] = root.attributes.getNamedItem("name").nodeValue + "|" +
							root.attributes.getNamedItem("birth").nodeValue + "-" + root.attributes.getNamedItem("death").nodeValue;
			treeid[index] = root.attributes.getNamedItem("id").nodeValue;
			
			if (root.childNodes.length > 1)
			{
				if (root.childNodes[3] != null) 
					fillData(root.childNodes[3], index * 2);
				if (root.childNodes[1] != null) 
					fillData(root.childNodes[1], index * 2 + 1);
			}
		}
		
		function parseData()
		{
			if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			
			xmlhttp.open("GET","data.php",false);
			xmlhttp.overrideMimeType('text/xml');
			xmlhttp.send();
			xmlDoc=xmlhttp.responseXML;
			
			// xmlDoc.getElementsByTagName("person")[0].attributes.getNamedItem("name").nodeValue
			var root = xmlDoc.getElementsByTagName("person")[0];
			getLevel(root, 0);
			fillData(root, 1);
		}
		
		
		function draw() {
			var clr1 = "rgb(188, 219, 253)", clr2 = "rgb(183, 230, 184)", clr3 = "rgb(255, 215, 213)", clr4 = "rgb(255, 255, 198)";
			var clr1_bdr = "rgb(6, 121, 249)", clr2_bdr = "rgb(62, 181, 65)", clr3_bdr = "rgb(255, 85, 77)", clr4_bdr = "rgb(200, 200, 0)";
			
			parseData();
			
			var canvas = document.getElementById("canvas");
			var ctx = canvas.getContext("2d");
			ctx.lineWidth = 2;
			
			
			for (var i = level; i >= 0; i --)
			{
				var total_cols = Math.pow(2, i);
				for (var j = 0; j < total_cols; j ++)
				{
					ctx.beginPath();
					
					ctx.fillStyle = clr1;
					ctx.strokeStyle = clr1_bdr;
					
					if ( i > 1 )
					{
						if ( j >= total_cols / 4)
						{
							ctx.fillStyle = clr2;
							ctx.strokeStyle = clr2_bdr;
						}
						
						if ( j >= total_cols / 2)
						{
							ctx.fillStyle = clr3;
							ctx.strokeStyle = clr3_bdr;
							
						}
						
						if ( j >= total_cols * 3 / 4)
						{
							ctx.fillStyle = clr4;
							ctx.strokeStyle = clr4_bdr;
						}
					}

					ctx.arc(675, 660 , radius[i] , (1 + j / total_cols) * Math.PI , (1 + (j + 1) / total_cols) * Math.PI, false);
					ctx.lineTo(675, 660);
					ctx.fill();
					ctx.stroke();
				}
			}
			
			/* axis - x */
			ctx.strokeStyle = clr1_bdr;
			ctx.beginPath();
			ctx.moveTo(675 - radius[level], 660);
			ctx.lineTo(675, 660);
			ctx.stroke();
			
			/* text */
			ctx.translate(675, 660);
			ctx.fillStyle = "black";
			ctx.font="9pt Arial Black";
					
			var k = Math.pow(2, level + 1) - 1;
			for (var i = level; i >= 4; i --)
			{
				var total_cols = Math.pow(2, i);
				ctx.save();
				ctx.beginPath();
				ctx.rotate( Math.PI / total_cols);
				var rad = (radius[i] + radius[i - 1]) / 2;
				
				for (var j = 0; j < total_cols; j ++)
				{
					if (j == total_cols / 2)
					{
						ctx.restore();
						ctx.rotate( - Math.PI / 2);
					}					
					
					if (treedata[k]){
						var sep = treedata[k --].split("|");
						var sep2 = sep[0].split(" ");
						
						if (j < total_cols / 2) 
						{
							if ( i == 4 )
							{	
								var wd = ctx.measureText(sep2[1]).width / 2;
								ctx.fillText(sep2[0], -rad - wd, 15);
								ctx.fillText(sep2[1], -rad - wd, 30);
								/*if (sep[1] != "-")
									ctx.fillText(sep[1], -rad - wd, 45);*/
							}else{
								var wd = ctx.measureText(sep[0]).width / 2;
								ctx.fillText(sep[0], -rad - wd, 15);
								/*if (sep[1] != "-")
									ctx.fillText(sep[1], -rad - wd, 30);*/
							}
						}else{
							if ( i == 4 )
							{
								var wd = ctx.measureText(sep2[1]).width / 2;
								ctx.fillText(sep2[0], rad - wd, 15);
								ctx.fillText(sep2[1], rad - wd, 30);
								/*if (sep[1] != "-")
									ctx.fillText(sep[1], rad - wd, 45);*/
							}else{
								var wd = ctx.measureText(sep[0]).width / 2;
								ctx.fillText(sep[0], rad - wd, 15);
								/*if (sep[1] != "-")
									ctx.fillText(sep[1], rad - wd, 30);*/
							}
						}
					}else
						k --;
					ctx.rotate( Math.PI / total_cols);
				}
			}
			
			var angle = new Array( - 1.5, -1.1, -0.7, -0.3, 0.07, 0.47, 0.87, 1.25);
			for (var j = 0; j < 8; j ++)
			{
				if (treedata[k])
				{
					var sep = treedata[k --].split("|");
					var sep2 = sep[0].split(" ");
					ctx.save();
					ctx.rotate(angle[j]);
					ctx.fillText(sep2[0], -5, -240);
					ctx.fillText(sep2[1], -8, -220);
					//ctx.fillText(sep[1], -6, -200);
					ctx.restore();
				}else
					k--;
			}		
			
			var angle = new Array( - 1.2, -0.45, 0.3, 1.1);
			for (var j = 0; j < 4; j ++)
			{
				if (treedata[k])
				{
					var sep = treedata[k --].split("|");
					var sep2 = sep[0].split(" ");
					ctx.save();
					ctx.rotate(angle[j]);
					ctx.fillText(sep2[0], -5, -160);
					ctx.fillText(sep2[1], -10, -145);
					//ctx.fillText(sep[1], -15, -130);
					ctx.restore();
				}else
					k--;
			}
			
			var angle = new Array( - 1.3, 0.2);
			for (var j = 0; j < 2; j ++)
			{
				if (treedata[k])
				{
					var sep = treedata[k --].split("|");
					var sep2 = sep[0].split(" ");
					ctx.save();
					ctx.rotate(angle[j]);
					ctx.fillText(sep2[0], -18, -100);
					ctx.rotate(0.45);
					ctx.fillText(sep2[1], -12, -100);
					ctx.rotate(0.4);
					if (sep2[2])
						ctx.fillText(sep2[2], -9, -100);
					//ctx.fillText(sep[1], -28, -85);
					ctx.restore();
				}else
					k--;
			}
			
			var sep = treedata[1].split("|");
			var sep2 = sep[0].split(" ");
			ctx.fillText(sep2[0] + " " + sep2[1], -50, -40);
			if (sep2[2])
				ctx.fillText(sep2[2], -40, -25);
			//ctx.fillText(sep[1], -45, -10);
			
			// Listener
			canvas.addEventListener("click", on_click, false);
		}
</script>
</head>

<body onLoad="draw()">
	<canvas id="canvas" width="1350" height="700"></canvas>
</body>
</html>  
