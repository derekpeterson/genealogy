<html>
	<head>
		<script>
		var circle = new Object();
                        circle.center = new Object();
		function init(){
			var canvas = document.getElementById('chart');
			var ctx = canvas.getContext('2d');
			canvas.width = window.innerWidth;
			canvas.height = window.innerHeight;

			//ctx.fillStyle='black';
			//ctx.fillRect(10,10,55,50);

			
			circle.center.x = canvas.width/2;
			circle.center.y = canvas.height-20;
			circle.radius = (canvas.width-(canvas.width*.1))/2;
			circle.rows = 9;

			circle.circumference = circle.radius*2*Math.PI/2;

			for(i = 1; i <= circle.rows; i++){
				for(c = 0; c < Math.pow(i,2); c++){
					angle = (180 / Math.pow(i,2)) * c;
					angle = Math.floor(angle);
					angle *= -1;
					console.log(angle);
					r = circle.radius / circle.rows * i;
					circumference = r*2*Math.PI/2;
					drawCell(circle.center.x+r*Math.cos(radians(angle)),circle.center.y+r*Math.sin(radians(angle)),(r/i)-3,(circumference/Math.pow(i,2))-2,angle);
				}
			}
		}
		function drawCell(x,y,width,height,angle){
			var canvas = document.getElementById('chart');
                        var ctx = canvas.getContext('2d');

			shape = new Object();
			shape.x = x;
			shape.y = y;
			shape.width = width;
			shape.height = height;
			shape.angle = angle;
			shape.radians = Math.atan2();
			ctx.beginPath();
			
			ctx.moveTo(shape.x,shape.y);	
			ctx.lineTo(shape.x+(shape.width*Math.cos(radians(shape.angle))),shape.y+(shape.width*Math.sin(radians(shape.angle))));
			ctx.lineTo(shape.x+(shape.width*Math.cos(radians(shape.angle)))+(shape.height*Math.cos(radians(shape.angle+90))),shape.y+(shape.width*Math.sin(radians(shape.angle)))+(shape.height*Math.sin(radians(shape.angle+90))));
			ctx.lineTo(shape.x+(shape.height*Math.cos(radians(shape.angle+90))),shape.y+(shape.height*Math.sin(radians(shape.angle+90))));
			ctx.lineTo(shape.x,shape.y);

			ctx.stroke();
		}
		function radians(degrees){
			return (Math.PI/180)*degrees;
		}
		</script>
	</head>
	<body onload='init()' style='margin:0px;width:100%;height:100%;'>
		<canvas id='chart'></canvas>
	</body>
</html>
